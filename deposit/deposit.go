package deposit

import (
	"github.com/lovoo/goka"
	"google.golang.org/protobuf/proto"
)

var (
	DepositStream goka.Stream = "deposit"
)

type DepositCodec struct{}

func (c *DepositCodec) Encode(value interface{}) ([]byte, error) {
	convertedValue := value.(proto.Message)
	return proto.Marshal(convertedValue)
}

func (c *DepositCodec) Decode(data []byte) (interface{}, error) {
	var d Deposit
	return &d, proto.Unmarshal(data, &d)
}
