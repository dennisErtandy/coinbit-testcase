# Coinbit Interview Testcase for Dennis Ertandy

## 1.Endpoints
The wallet service offers two endpoints:

1. `localhost:8080/deposit` (POST)
    1. Request body

    ```json
    {
        "wallet_id":"1234",
        "amount": "500.50"
    }
    ```
    2. Response
    ```
    {
    "data": {
        "wallet_id": "1234",
        "amount": "500.5"
    },
    "message": "",
    "status": "Success"
    }
    ```
2. `localhost:8080/{wallet_id}/details`(GET)
    1. Response
    ```
    {
    "data": {
        "wallet_id": "1234",
        "balance": "500.5",
        "above_threshold": false
    },
    "message": "",
    "status": "Success"
    }
    ```
    
##  2. Start up service
1. To start services Kafka, Redis, and Zookeeper should be started first. Run command below in terminal to start.
```sh
docker-compose up -d
```
to stop Kafka, Redis, and Zookeeper run command below in terminal
```sh
docker-compose down
```
2. Initialize Kafka topics by running commands below
```sh
docker-compose exec broker kafka-topics --create --bootstrap-server \
localhost:9092 --replication-factor 1 --partitions 1 --topic balance-table
```
```sh
docker-compose exec broker kafka-topics --create --bootstrap-server \
localhost:9092 --replication-factor 1 --partitions 1 --topic aboveThreshold-table
```
```sh
docker-compose exec broker kafka-topics --create --bootstrap-server \
localhost:9092 --replication-factor 1 --partitions 1 --topic deposit
```
3.Start the endpoint handler, emitter, and view by running command below
```sh
go run cmd/service/main.go
```
4. In different terminal window, start balance processor
```sh
go run cmd/processor/main.go -balance 
```
5. In different terminal window, start above-threshold processor
```sh
go run cmd/processor/main.go -above-threshold
```



