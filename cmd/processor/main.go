package main

import (
	aboveThreshold "coinbit-transaction/processor/above-threshold"
	"coinbit-transaction/processor/balance"
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"golang.org/x/sync/errgroup"
)

var (
	brokers           = []string{"localhost:9092"}
	runBalance        = flag.Bool("balance", false, "run filter processor")
	runAboveThreshold = flag.Bool("above-threshold", false, "run filter processor")
)

func main() {
	flag.Parse()
	ctx, cancel := context.WithCancel(context.Background())
	grp, ctx := errgroup.WithContext(ctx)

	if *runBalance {
		log.Println("starting balance")
		grp.Go(balance.Run(ctx, brokers))
	}
	if *runAboveThreshold {
		log.Println("starting above-threshold")
		grp.Go(aboveThreshold.Run(ctx, brokers))
	}
	// Wait for SIGINT/SIGTERM
	waiter := make(chan os.Signal, 1)
	signal.Notify(waiter, syscall.SIGINT, syscall.SIGTERM)
	select {
	case <-waiter:
	case <-ctx.Done():
	}
	cancel()
	if err := grp.Wait(); err != nil {
		log.Println(err)
	}
	log.Println("done")
}
