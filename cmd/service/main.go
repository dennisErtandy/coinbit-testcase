package main

import (
	"coinbit-transaction/deposit"
	"coinbit-transaction/service"
	"flag"
)

var (
	broker = flag.String("broker", "localhost:9092", "boostrap Kafka broker")
)

func main() {
	service.Run([]string{*broker}, deposit.DepositStream)

}
