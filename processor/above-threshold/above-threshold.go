package aboveThreshold

import (
	"coinbit-transaction/deposit"
	"coinbit-transaction/lib/util"
	"context"
	"log"
	"strconv"

	"time"

	"github.com/go-redis/redis"
	"github.com/lovoo/goka"
	"google.golang.org/protobuf/proto"
)

var (
	group goka.Group = "aboveThreshold"
	Table goka.Table = goka.GroupTable(group)
	rdb   *redis.Client
)

// threshold duration
const thresholdTimeWindow time.Duration = 2 * time.Minute
const thresholdLimit float64 = 10000

type AboveThresholdCodec struct{}

func (c *AboveThresholdCodec) Encode(value interface{}) ([]byte, error) {
	convertedValue := value.(proto.Message)
	return proto.Marshal(convertedValue)
}

func (c *AboveThresholdCodec) Decode(data []byte) (interface{}, error) {
	var b AboveThreshold
	return &b, proto.Unmarshal(data, &b)
}

func updateThrehshold(ctx goka.Context, dpsit interface{}) {
	var at *AboveThreshold
	var finalThreshold float64
	d := dpsit.(*deposit.Deposit)
	existingThreshold, err := rdb.Get(d.WalletId).Result()
	if err != nil {
		if err == redis.Nil {
			// adding threshold for 2 minutes
			err = rdb.Set(d.WalletId, d.Amount, thresholdTimeWindow).Err()
			if err != nil {
				log.Fatal(err)
			}
			finalThreshold = float64(d.Amount)
		}
	} else {
		//update current threshold
		thresholdAmount, _ := strconv.ParseFloat(existingThreshold, 64)
		thresholdAmount += float64(d.Amount)
		err = rdb.Set(d.WalletId, thresholdAmount, thresholdTimeWindow).Err()
		if err != nil {
			log.Fatal(err)
		}
		finalThreshold = thresholdAmount
	}
	at = new(AboveThreshold)
	if util.IsBigger(finalThreshold, thresholdLimit) {
		at.WalletId = d.WalletId
		at.AboveThreshold = true
		ctx.SetValue(at)
	} else {
		at.WalletId = d.WalletId
		at.AboveThreshold = false
		ctx.SetValue(at)
	}

}
func Run(ctx context.Context, brokers []string) func() error {
	return func() error {
		g := goka.DefineGroup(group,
			goka.Input(deposit.DepositStream, new(deposit.DepositCodec), updateThrehshold),
			goka.Persist(new(AboveThresholdCodec)),
		)
		rdb = redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "",
			DB:       0,
		})
		p, err := goka.NewProcessor(brokers, g)
		if err != nil {
			return err
		}
		return p.Run(ctx)
	}
}
