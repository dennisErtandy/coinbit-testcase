package balance

import (
	"coinbit-transaction/deposit"
	"context"

	"github.com/lovoo/goka"
	"google.golang.org/protobuf/proto"
)

var (
	group goka.Group = "balance"
	Table goka.Table = goka.GroupTable(group)
)

type BalanceCodec struct{}

func (c *BalanceCodec) Encode(value interface{}) ([]byte, error) {
	convertedValue := value.(proto.Message)
	return proto.Marshal(convertedValue)
}

func (c *BalanceCodec) Decode(data []byte) (interface{}, error) {
	var b Balance
	return &b, proto.Unmarshal(data, &b)
}

func updateBalance(ctx goka.Context, dpsit interface{}) {
	var b *Balance

	d := dpsit.(*deposit.Deposit)
	if v := ctx.Value(); v != nil {
		depositAmount := d.GetAmount()
		b := v.(*Balance)
		b.Balance += depositAmount
		ctx.SetValue(b)

	} else {
		// create new if not existed
		b = new(Balance)
		b.WalletId = d.GetWalletId()
		b.Balance = d.GetAmount()
		ctx.SetValue(b)
	}

}
func Run(ctx context.Context, brokers []string) func() error {
	return func() error {
		g := goka.DefineGroup(group,
			goka.Input(deposit.DepositStream, new(deposit.DepositCodec), updateBalance),
			goka.Persist(new(BalanceCodec)),
		)
		p, err := goka.NewProcessor(brokers, g)
		if err != nil {
			return err
		}
		return p.Run(ctx)
	}
}
