package service

import (
	"coinbit-transaction/deposit"
	"coinbit-transaction/lib/util"
	aboveThreshold "coinbit-transaction/processor/above-threshold"
	"coinbit-transaction/processor/balance"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/lovoo/goka"
)

func Run(brokers []string, stream goka.Stream) {
	balanceView, err := goka.NewView(brokers, balance.Table, new(balance.BalanceCodec))
	if err != nil {
		panic(err)
	}
	go balanceView.Run(context.Background())

	aboveThresholdView, err := goka.NewView(brokers, aboveThreshold.Table, new(aboveThreshold.AboveThresholdCodec))
	if err != nil {
		panic(err)
	}
	go aboveThresholdView.Run(context.Background())

	emitter, err := goka.NewEmitter(brokers, stream, new(deposit.DepositCodec))
	if err != nil {
		panic(err)
	}
	defer emitter.Finish()

	router := mux.NewRouter()
	router.HandleFunc("/deposit", depositHandler(emitter, stream)).Methods("POST")
	router.HandleFunc("/{wallet_id}/details", detailHandler(balanceView, aboveThresholdView)).Methods("GET")
	log.Printf("Listen port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func depositHandler(emitter *goka.Emitter, stream goka.Stream) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var d deposit.Deposit

		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			util.SendBadRequest(w, err.Error())
			return
		}

		err = json.Unmarshal(b, &d)
		if err != nil {
			util.SendBadRequest(w, err.Error())
			return
		}

		err = emitter.EmitSync(d.WalletId, &d)

		if err != nil {
			util.SendBadRequest(w, err.Error())
			return
		}
		log.Printf("money deposit: %v", &d)
		util.SendSuccess(w, &d)
	}
}

func detailHandler(balanceView *goka.View, aboveThresholdView *goka.View) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		type detailResponse struct {
			WalletID       string  `json:"wallet_id"`
			Balance        float64 `json:"balance,string"`
			AboveThreshold bool    `json:"above_threshold"`
		}
		walletID := mux.Vars(r)["wallet_id"]
		balanceValue, _ := balanceView.Get(walletID)
		if balanceValue == nil {
			util.SendBadRequest(w, "wallet not found")
			return
		}
		balance := balanceValue.(*balance.Balance)

		aboveThresholdVal, _ := aboveThresholdView.Get(walletID)
		if aboveThresholdVal == nil {
			util.SendBadRequest(w, "wallet not found")
			return
		}
		aboveThreshold := aboveThresholdVal.(*aboveThreshold.AboveThreshold)

		response := detailResponse{
			WalletID:       walletID,
			Balance:        float64(balance.Balance),
			AboveThreshold: aboveThreshold.AboveThreshold,
		}

		util.SendSuccess(w, response)

	}
}
