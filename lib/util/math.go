package util

import "math/big"

func IsBigger(value1 float64, value2 float64) bool {
	var bigFloat1 = big.NewFloat(value1)
	var bigFloat2 = big.NewFloat(value2)
	result := bigFloat1.Cmp(bigFloat2)
	// value1 bigger than value2
	if result > 0 {
		return true
	} else {
		// value1 less than or equal value2
		return false
	}
}
