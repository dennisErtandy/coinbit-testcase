package util

import (
	"encoding/json"
	"net/http"
)

// SendSuccess send success into response with 200 http code.
func SendSuccess(w http.ResponseWriter, payload interface{}) {
	RespondWithJSON(w, 200, payload, "", "Success")
}

// SendBadRequest send bad request into response with 400 http code.
func SendBadRequest(w http.ResponseWriter, message string) {
	emptyJSONArr := []map[string]interface{}{}
	RespondWithJSON(w, 400, emptyJSONArr, message, "Failed")
}

// RespondWithJSON write json response format
func RespondWithJSON(
	w http.ResponseWriter,
	httpCode int,
	payload interface{},
	message string,
	status string,

) {
	respPayload := map[string]interface{}{
		"status":  status,
		"message": message,
		"data":    payload,
	}

	response, _ := json.Marshal(respPayload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpCode)
	_, _ = w.Write(response)
}
